# NeoVim configuration bundle

## Features

A basic [Vim Bootstrap](https://vim-bootstrap.com/) config enhanced with LanguageServer support (mostly including installation and configuration of the LanguageServers)

### Supported languages

Or, rather, languageservers

* [x] [CCLS](https://github.com/MaskRay/ccls) used for C, C++, Objective-C and CUDA
* [x] [Bash](https://github.com/mads-hartmann/bash-language-server)
* [ ] [C#](https://github.com/CXuesong/LanguageServer.NET)
* [ ] [CSS](https://github.com/Microsoft/vscode/tree/master/extensions/css)
* [ ] [Dockerfile](https://github.com/rcjsuen/dockerfile-language-server-nodejs)
* [ ] [GoLang](https://github.com/sourcegraph/go-langserver)
* [ ] [Groovy](https://github.com/prominic/groovy-language-server)
* [ ] [HTML](https://github.com/Microsoft/vscode/tree/master/extensions/html)
* [ ] [Java](https://github.com/eclipse/eclipse.jdt.ls)
* [ ] [Javascript & Typescript](https://github.com/sourcegraph/javascript-typescript-langserver)
* [ ] [JSON](https://github.com/Microsoft/vscode/tree/master/extensions/json)
* [ ] [SonarLint](https://github.com/SonarSource/sonarlint-core)
* [ ] [Kotlin](https://github.com/fwcd/kotlin-language-server)
* [ ] [LaTeX](https://github.com/latex-lsp/texlab)
* [ ] [LUA](https://github.com/EmmyLua/EmmyLua-LanguageServer)
* [ ] [PHP](https://github.com/felixfbecker/php-language-server)
* [ ] [Python](https://github.com/Microsoft/python-language-server)
* [ ] [Objective-C and Swift](https://github.com/apple/sourcekit-lsp)
* [ ] [Terraform](https://github.com/juliosueiras/terraform-lsp)
* [ ] [XML](https://github.com/angelozerr/lsp4xml)
* [ ] [YAML](https://github.com/redhat-developer/yaml-language-server)

##### Not sure if can be implemented, but should be

* [ ] [Ansible](https://github.com/VSChina/vscode-ansible)

##### TODO

* [ ] Arduino (PlatformIO) support. There seems to be some kind of ready plugin implementations for this but I didn't get it working right away. Might roll-my-own.

### Authors

Axel Latvala
