#!/bin/bash
NVIM_BASEINSTALLDIR="/opt/nvim"
NVIM_CONFIGDIR=~/.config/nvim
curl -LO https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
chmod +x nvim.appimage
NVIM_VER=$(./nvim.appimage --version | head -1 | awk '{print $2}')
sudo mkdir -p "$NVIM_BASEINSTALLDIR/$NVIM_VER"
sudo mv nvim.appimage "$NVIM_BASEINSTALLDIR/$NVIM_VER/nvim"
sudo ln -sf "$NVIM_BASEINSTALLDIR/$NVIM_VER" "$NVIM_BASEINSTALLDIR/current"
sudo ln -sf "$NVIM_BASEINSTALLDIR/current/nvim" /usr/local/bin/nvim
mkdir -p "$NVIM_CONFIGDIR"
cp resources/init.vim "$NVIM_CONFIGDIR/"
sudo apt update && sudo apt install -y python-pip
pip install neovim
nvim +PlugInstall +qa
./languageserver/ccls.sh
./languageserver/bash-language-server.sh
